//
//  ViewController.swift
//  Main Project
//
//  Created by Chapter iOS on 29/01/20.
//  Copyright © 2020 Saminos. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        #if staging
        print("staging")
        #elseif debug
        print("debug")
        #elseif release
        print("release")
        #else
        print("else")
        #endif
    }


}

