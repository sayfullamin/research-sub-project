//
//  AppDelegate.swift
//  Main Project
//
//  Created by Chapter iOS on 29/01/20.
//  Copyright © 2020 Saminos. All rights reserved.
//

import UIKit

import MCore
import MServices
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = .init(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = ViewController()
        MCore.something()
        
        MServices.callMCoreSomething()
        return true
    }



}

