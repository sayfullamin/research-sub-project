//
//  MServices.swift
//  MServices
//
//  Created by Chapter iOS on 29/01/20.
//  Copyright © 2020 Saminos. All rights reserved.
//

import MCore
public class MServices {
    public static func callMCoreSomething() {
        print("begin call MCore.something")
        MCore.something()
        print("end of call MCore.something")
    }
}
